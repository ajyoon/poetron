const fs = require('fs');
const bml = require('bml');
const express = require('express');
const app = express();
const port = 3000;

let scriptPath = process.argv[2];
let content = fs.readFileSync(scriptPath, 'utf8');

app.get('/seed/:seed', (req, res) => {
  let seed = Number(req.params.seed);
  let result = bml(content, {randomSeed: seed});
  res.send({result: result});
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
