if [ "$#" -ne 1 ]; then
    echo "Illegal number of parameters"
    exit 1
fi

rsync -avz \
      --exclude="*.git" \
      --exclude="*.pyc" \
      --exclude="__pycache__" \
      --exclude="log" \
      ../../../../poetron \
      pi@$1:~/
