import os
import random
from datetime import datetime

from blur.rand import weighted_choice, weighted_rand
import sh

from poetron.formatter import Run, Alignment

DIR = os.path.dirname(os.path.abspath(__file__))


STANZA_ALIGNMENT_WEIGHTS = [
    (Alignment.LEFT, 4),
    (Alignment.RIGHT, 1),
]

STANZA_LEFT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 2)
]

STANZA_RIGHT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 2)
]


def make_poem(seed):
    bml_content = load_bml()
    generated_text = sh.bml('--seed', seed, _in=bml_content)
    body = random_format_poem_body(generated_text)
    return finalize_poem(body, seed)


def load_bml():
    return load_main_bml() + '\n' + load_ending_bml()


def load_main_bml():
    path = os.path.join(DIR, 'text.bml')
    with open(path) as f:
        return f.read()


def load_ending_bml():
    ending_number = random.randint(1, 4)
    path = os.path.join(DIR, f'ending_{ending_number}.bml')
    with open(path) as f:
        return f.read()


def random_format_poem_body(text):
    stanzas = [s + '\n' for s in text.split('\n\n')]
    runs = []
    for stanza in stanzas:
        alignment = weighted_choice(STANZA_ALIGNMENT_WEIGHTS)
        left_indent = int(weighted_rand(STANZA_LEFT_INDENT_WEIGHTS))
        right_indent = int(weighted_rand(STANZA_RIGHT_INDENT_WEIGHTS))
        runs.append(Run(stanza, alignment, left_indent, right_indent))
    return runs


def finalize_poem(body_runs, seed):
    return [
        Run('againagain                       a dynamic poem'),
        Run.horizontal_line(),
        Run.blank(),
        *body_runs,
        Run.blank(),
        Run.horizontal_line(),
        Run.blank(),
        Run('version {:010d} copyright (c) {} by'.format(
            seed, datetime.now().year)),
        Run.blank(),
        Run.blank(),
        Run('______________________________'),
        Run('(your name here)'),
        Run.blank(),
        Run.blank(),
        Run('andrew yoon\nandrewyoon.art\n@andrewjyoon',
            alignment=Alignment.RIGHT)
    ]
