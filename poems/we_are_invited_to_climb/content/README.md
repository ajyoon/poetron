# Excerpt from _We Are Invited to Climb_

This is an excerpt from the book of poetry _We Are Invited to Climb_, published by Awst Press under a Creative Commons NY-NC-SA license. The excerpt has been truncated and modified to suit the purposes of this poetron integration. You can find the complete source code for the book at https://gitlab.com/ajyoon/weareinvitedtoclimb.org/
