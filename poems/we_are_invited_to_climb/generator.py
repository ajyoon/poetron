import os
import random
from datetime import datetime
import re
import requests

import subprocess

from blur.rand import weighted_choice, weighted_rand

from poetron.formatter import Run, Alignment

DIR = os.path.dirname(os.path.abspath(__file__))

STANZA_ALIGNMENT_WEIGHTS = [
    (Alignment.LEFT, 4),
    (Alignment.RIGHT, 1),
]

STANZA_LEFT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 2)
]

STANZA_RIGHT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 0)
]


def extract_poem_text(raw_text):
    """
    The input BML script is a truncated version of the complete
    book content. In order to keep the RNG synchronized with the
    live website, we have to include all text preceding the poem.

    I modified the BML script to strip out unnecessary (non-chancy)
    text from the preceding code, and to strip HTML tags and add a
    convenience sentinel.
    """
    lines = raw_text.splitlines()
    poem_lines = lines[lines.index('>>>>POEM BEGINS<<<<') + 1:]
    return '\n'.join(poem_lines)


def make_poem(seed):
    response = requests.get(f'http://localhost:3000/seed/{seed}')
    assert(response.status_code == 200)
    generated_text = response.json()['result']
    cleaned_up_text = extract_poem_text(generated_text)
    body = random_format_poem_body(cleaned_up_text)
    return finalize_poem(body, seed)


def random_format_poem_body(text):
    stanzas = [s + '\n' for s in text.split('\n\n')]
    runs = []
    for stanza in stanzas:
        alignment = weighted_choice(STANZA_ALIGNMENT_WEIGHTS)
        left_indent = int(weighted_rand(STANZA_LEFT_INDENT_WEIGHTS))
        right_indent = int(weighted_rand(STANZA_RIGHT_INDENT_WEIGHTS))
        runs.append(Run(stanza, alignment, left_indent, right_indent))
    return runs


def finalize_poem(body_runs, seed):
    return [
        Run('we are invited to climb'),
        Run('a chance poem', alignment=Alignment.RIGHT),
        Run.horizontal_line(),
        Run.blank(),
        Run.blank(),
        *body_runs,
        Run.blank(),
        Run.blank(),
        Run.horizontal_line(),
        Run.blank(),
        Run.blank(),
        Run('from a book of chance poetry of the same name'),
        Run(f'weareinvitedtoclimb.org/version/{seed}'),
        Run.blank(),
        Run('cc by-nc-sa (some rights reserved)'),
        Run('source code at andrewyoon.art/poetron'),
        Run.blank(),
        Run('andrewyoon.art\n@andrewyooon',
            alignment=Alignment.RIGHT)
    ]
