# other kinds of mirrors

the helga davis quote is from her podcast, _helga_, [in conversation with jacqueline woodson](https://www.newsounds.org/shows/helga/1) around 43:40.

> it's a very important thing right now that we have other kinds of mirrors to hold up to the world, and to ourselves, because even if you aren't involved in politics, or in any of that, there is still a person who climbs out of bed in the morning and looks in a mirror and says words, has words for what they see, has feelings about what they see, and i think it's so important to be in the habit of making that experience a positive one, so that we can make better choices, so that we can love better, we can be better humans

to generate and display an instance of the poem without a receipt printer or input button, run `python test_generate.py`
