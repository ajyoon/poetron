import logging
import os
import sys

from poetron.button import Button
from poetron.printer import Printer
from poetron import random_seed

import generator

ROOT_DIR = os.path.dirname(os.path.realpath(__file__))

printer = Printer()


def setup_logging():
    root = logging.getLogger()
    root.setLevel(logging.INFO)


def make_and_print_poem():
    seed = random_seed.new_seed()
    poem_runs = generator.make_poem(seed)
    printer.print_runs(poem_runs)


def main_loop():
    Button().event_loop(make_and_print_poem)


def main():
    setup_logging()
    logging.info("Launching application")
    try:
        main_loop()
    except Exception as e:
        logging.exception(
            "Got unhandled error, quitting (systemd should restart).")
        raise e


if __name__ == '__main__':
    main()
