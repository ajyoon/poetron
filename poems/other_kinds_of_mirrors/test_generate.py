import random
import time

from poetron import random_seed

import generator

result = generator.make_poem(random_seed.new_seed())
for run in result:
    print(run.render())
