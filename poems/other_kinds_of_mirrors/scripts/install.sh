echo "Setting up systemd service"

sudo cp mirrors.service /lib/systemd/system/mirrors.service
sudo chmod 644 /lib/systemd/system/mirrors.service
sudo systemctl daemon-reload
sudo systemctl enable mirrors.service
sudo systemctl restart mirrors.service
sudo systemctl status mirrors.service
