import os
import random
from datetime import datetime
import re

from blur.rand import weighted_choice, weighted_rand
import sh

from poetron.formatter import Run, Alignment

DIR = os.path.dirname(os.path.abspath(__file__))

STANZA_ALIGNMENT_WEIGHTS = [
    (Alignment.LEFT, 4),
    (Alignment.RIGHT, 1),
]

STANZA_LEFT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 2)
]

STANZA_RIGHT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 0)
]


def make_poem(seed):
    generated_text = sh.bml('--seed', seed, os.path.join(DIR, 'content.bml'))
    cleaned_up_text = re.sub(' +', ' ', str(generated_text)).strip()
    body = random_format_poem_body(cleaned_up_text)
    return finalize_poem(body, seed)


def random_format_poem_body(text):
    stanzas = [s + '\n' for s in text.split('\n\n')]
    runs = []
    for stanza in stanzas:
        alignment = weighted_choice(STANZA_ALIGNMENT_WEIGHTS)
        left_indent = int(weighted_rand(STANZA_LEFT_INDENT_WEIGHTS))
        right_indent = int(weighted_rand(STANZA_RIGHT_INDENT_WEIGHTS))
        runs.append(Run(stanza, alignment, left_indent, right_indent))
    return runs


def finalize_poem(body_runs, seed):
    return [
        Run('other kinds of mirrors'),
        Run('a chance poem', alignment=Alignment.RIGHT),
        Run.horizontal_line(),
        Run.blank(),
        Run.blank(),
        *body_runs,
        Run.blank(),
        Run.blank(),
        Run.horizontal_line(),
        Run.blank(),
        Run.blank(),
        Run(f'seed {seed}'),
        Run('source code at andrewyoon.art/poetron'),
        Run('CC 0: No Rights Reserved'),
        Run.blank(),
        Run('andrew yoon\nandrewyoon.art\n@andrewjyoon',
            alignment=Alignment.RIGHT)
    ]
