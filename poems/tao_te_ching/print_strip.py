from poetron.button import Button
from poetron.printer import Printer
from poetron import random_seed
import sys
import subprocess
import random
import re

import generator

CM_PER_LINE = 0.359375

requested_cm = int(sys.argv[1])
requested_lines = int(requested_cm / CM_PER_LINE)


text = open('content/fullsource.bml').read()
# remove html tags
cleaned_text = re.sub('<.*?>', '', text)
result = generator.random_format_poem_body(cleaned_text)

rendered_text = '\n'.join([run.render() for run in result])
# have to split and join one more time..
rendered_lines = rendered_text.splitlines()

total_line_count = len(rendered_lines)
max_offset = total_line_count - requested_lines
start_offset = random.randint(0, max_offset)

selected_lines = rendered_lines[start_offset : start_offset + requested_lines]

final_text = '\n'.join(selected_lines)

printer = Printer()
printer.print_raw_text(final_text)

# print(final_text)

