import os
import random
from datetime import datetime
import re
import requests

import subprocess

from blur.rand import weighted_choice, weighted_rand

from poetron.formatter import Run, Alignment

DIR = os.path.dirname(os.path.abspath(__file__))

STANZA_ALIGNMENT_WEIGHTS = [
    (Alignment.LEFT, 4),
    (Alignment.RIGHT, 1),
]

STANZA_LEFT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 2)
]

STANZA_RIGHT_INDENT_WEIGHTS = [
    (0, 10),
    (3, 3),
    (20, 0)
]


def make_poem(seed):
    response = requests.get(f'http://localhost:3000/seed/{seed}')
    assert(response.status_code == 200)
    generated_text = response.json()['result']
    body_runs = [Run(line, wrap_indent=4) for line in generated_text.splitlines()]
    return finalize_poem(body_runs, seed)


def finalize_poem(body_runs, seed):
    return [
        Run('The Tao Te Ching, Chapter 15'),
        Run('a chancelation', alignment=Alignment.RIGHT),
        Run.horizontal_line(),
        Run.blank(),
        Run.blank(),
        *body_runs,
        Run.blank(),
        Run.blank(),
        Run.horizontal_line(),
        Run.blank(),
        Run.blank(),
        Run('from a work-in-progress chancelation'),
        Run(f'at seed {seed}'),
        Run('out of some 10^30 possibilities'),
        Run.blank(),
        Run('cc by-sa (some rights reserved)'),
        Run('source code at andrewyoon.art/poetron'),
        Run.blank(),
        Run('andrewyoon.art\n@andrewyooon',
            alignment=Alignment.RIGHT)
    ]
