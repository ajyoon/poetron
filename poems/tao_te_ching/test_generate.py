from poetron import random_seed

import generator

result = generator.make_poem(1234)

for run in result:
    print(run.render())
