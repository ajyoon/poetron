echo "Setting up systemd service"

sudo cp bml_server.service /lib/systemd/system/
sudo chmod 644 /lib/systemd/system/bml_server.service

sudo cp poetron.service /lib/systemd/system/poetron.service
sudo chmod 644 /lib/systemd/system/poetron.service

sudo systemctl daemon-reload

sudo systemctl enable bml_server.service
sudo systemctl restart bml_server.service

sudo systemctl enable poetron.service
sudo systemctl restart poetron.service
