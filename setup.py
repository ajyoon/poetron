from distutils.core import setup

setup(name='Poetron',
      version='0.2',
      description='Poetry generating robot',
      author='Andrew Yoon',
      author_email='andrew@nothing-to-say.org',
      packages=['poetron'],
      python_requires='>=3.6'
)
