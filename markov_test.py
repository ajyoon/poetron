from blur.markov.graph import Graph

from generator import BML_TEXT_PATH
from printer import Printer

WORD_COUNT = 2000

distance_weights = {
    1: 10,
    2: 1,
    3: 1,
    15: 1,
    -3: 2,
}

graph = Graph.from_file(BML_TEXT_PATH, distance_weights=distance_weights)

words = [graph.pick().value for _ in range(WORD_COUNT)]

text = ' '.join(words)

Printer().text(text)
