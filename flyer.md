## againagain

### a dynamic poem

this is a poem that changes. press the button to create and print a rendition that is yours. write your name at the bottom to claim its copyright.

not counting layout and capitalization, there are some 1,287,881,175,859,200,000 possible versions of this poem. we are asked---who owns this text?

the source code and text for this work is Free and available at gitlab.com/ajyoon/againagain

---

andrew yoon is a composer, poet, programmer, and pianist currently involved in using colors and painting to make sound, making poems that change, live coding music, playing double melodica, and growing community through his non-profit arts journal, nothing-to-say.org. he lives online at andrewyoon.art and @andrewjyoon
