from time import time
import os

import evdev
import logging

from poetron import config


class Button:
    def __init__(self, path=config.DEVICE_PATH):
        self.device = evdev.InputDevice(config.DEVICE_PATH)

    def event_loop(self, button_callback):
        hold_start_time = None
        for event in self.device.read_loop():
            logging.info(event)

            if self.is_button_press(event):
                hold_start_time = time()
            elif self.is_button_release(event):
                if (hold_start_time
                        and time() - hold_start_time
                        > config.SHUTDOWN_HOLD_LENGTH):
                    logging.info("Got shutdown signal, attempting to shut down.")
                    os.system("shutdown now -h")
                    return
                hold_start_time = None
                button_callback()
                # Prevent queued up event from button mashing causing
                # a ton of paper to be wasted
                self.flush_event_queue()

    def is_button_press(self, event):
        return (event.code == config.KEY_EVENT_CODE
                and event.value == config.BUTTON_PRESS)


    def is_button_release(self, event):
        return (event.code == config.KEY_EVENT_CODE
                and event.value == config.BUTTON_RELEASE)


    def flush_event_queue(self):
        try:
            for event in self.device.read():
                logging.info('skipping queued event %s', event)
        except BlockingIOError:
            pass
