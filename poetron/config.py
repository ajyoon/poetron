"""
Printer settings

These constants will vary based on environment and printer

Appropriate values can be found with `lsusb -vvv`

See also https://python-escpos.readthedocs.io/en/latest/user/installation.html#setup-udev-for-usb-printers
"""
VENDOR_ID = 0x04b8
PRODUCT_ID = 0x0e15
# Under "Endpoint Descriptor" -> bEndpointAddress labeled EP 1 IN
IN_ENDPOINT_ADDRESS = 0x82
# Under "Endpoint Descriptor" -> bEndpointAddress labeled EP 3 OUT
OUT_ENDPOINT_ADDRESS = 0x01

PRINTER_LINE_LENGTH = 47

_APPROX_LINE_HEIGHT_MM = 4
_APPROX_PRINTING_MM_PER_SEC = 100
APPROX_TIME_PER_LINE_S = _APPROX_LINE_HEIGHT_MM / _APPROX_PRINTING_MM_PER_SEC


"""
Evdev constants for button interface. Values will vary across setups

See https://python-evdev.readthedocs.io/en/latest/usage.html
"""
DEVICE_PATH = '/dev/input/event0'
KEY_EVENT_CODE = 288
BUTTON_PRESS = 1
BUTTON_RELEASE = 0
SHUTDOWN_HOLD_LENGTH = 10
