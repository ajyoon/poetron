import re
from enum import Enum, auto

import logging

from poetron import config


class Alignment(Enum):
    LEFT = auto()
    RIGHT = auto()
    # CENTER = auto()  # not supported rn


class Run:
    def __init__(self, text, alignment=Alignment.LEFT,
                 left_indent=0, right_indent=0, wrap_indent=0):
        self.text = text
        self.alignment = alignment
        self.left_indent = left_indent
        self.right_indent = right_indent
        self.wrap_indent = wrap_indent

    @classmethod
    def horizontal_line(cls, char='-'):
        return cls(char * config.PRINTER_LINE_LENGTH)

    @classmethod
    def blank(cls):
        return cls('')

    @property
    def run_line_length(self):
        return (config.PRINTER_LINE_LENGTH
                - self.left_indent - self.right_indent)

    def align(self, line):
        if self.alignment == Alignment.LEFT:
            spaces = self.left_indent
        else:
            spaces = (config.PRINTER_LINE_LENGTH
                      - (len(line) + self.right_indent))
        return (' ' * spaces) + line

    def render(self):
        line_chunks = []
        lines = []
        pos = 0
        for chunk in re.split(r'([\s])', self.text):
            if chunk == '\n':
                # Line break in the text
                lines.append(''.join(line_chunks))
                line_chunks = []
                pos = 0
                continue
            elif pos + len(chunk) > self.run_line_length:
                # Automatically inserted wrap
                lines.append(''.join(line_chunks))
                line_chunks = [' ' * self.wrap_indent]
                pos = self.wrap_indent
            line_chunks.append(chunk)
            pos += len(chunk)

        if line_chunks:
            lines.append(''.join(line_chunks))

        # Plain text alignment
        aligned_lines = [self.align(line) for line in lines]
        return '\n'.join(aligned_lines)

    def __str__(self):
        return f'Line: {self.__dict__}'
