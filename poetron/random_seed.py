import random


def new_seed(max_val=1000000000):
    set_seed(random.randint(0, max_val))
    return current_seed


def set_seed(seed):
    global current_seed
    current_seed = seed
    random.seed(current_seed)


current_seed = 0

new_seed()
