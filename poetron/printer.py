import re
import time

from escpos.printer import Usb
import logging

from poetron.formatter import Alignment, Run
from poetron import config


class Printer:
    def __init__(self):
        self.printer = Usb(
            config.VENDOR_ID, config.PRODUCT_ID,
            in_ep=config.IN_ENDPOINT_ADDRESS,
            out_ep=config.OUT_ENDPOINT_ADDRESS)
        
    def print_raw_text(self, text):
        self.printer.text(text)
        self.printer.cut()
        # Block approximately until printing is complete
        sleep_time = config.APPROX_TIME_PER_LINE_S * text.count('\n')
        logging.info('sleeping for %f seconds', sleep_time)
        time.sleep(sleep_time)
        logging.info('waking up after printing')

    def print_runs(self, runs):
        rendered = '\n'.join([run.render() for run in runs])
        final_text = self._vertically_pad(rendered)
        self.print_raw_text(final_text)

    def _vertically_pad(self, text):
        return ('\n' * 3) + text + ('\n' * 7)
